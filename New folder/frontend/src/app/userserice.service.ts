import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UsersericeService {
  constructor(private http: HttpClient) {}

  getUserData(username: string, password: string) {
    return this.http.get(
      'http://localhost:5558/user/' + username + '/' + password
    );
  }
}
