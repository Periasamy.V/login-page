import { Component } from '@angular/core';
import { UsersericeService } from '../userserice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  model: any = {};
  getData: any;
  constructor(private userservice: UsersericeService, private router: Router) {}
  ngOnInit() {}
  loginUser() {
    var user = this.model.username;
    var password = this.model.password;
    this.userservice.getUserData(user, password).subscribe((res: any) => {
      this.getData = res;

      if (this.getData == 1) {
        this.router.navigate(['/home']);
      } else {
        alert('invalid user');
      }
    });
  }
}
